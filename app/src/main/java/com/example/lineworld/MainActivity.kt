package com.example.lineworld

import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CaptureRequest
import android.os.Build
import android.os.Bundle
import android.widget.FrameLayout
import boofcv.alg.feature.detect.edge.EdgeContour
import boofcv.android.VisualizeImageData
import boofcv.android.camera2.VisualizeCamera2Activity
import boofcv.concurrency.BoofConcurrency
import boofcv.factory.feature.detect.edge.FactoryEdgeDetectors
import boofcv.struct.image.GrayS16
import boofcv.struct.image.GrayU8
import boofcv.struct.image.ImageBase
import boofcv.struct.image.ImageType
import com.google.vr.sdk.base.GvrView


class MainActivity : VisualizeCamera2Activity(), GvrRenderer.GvrRendererEvents {
    private var gvrRenderer: GvrRenderer? = null
    private var surfaceTexture: SurfaceTexture? = null
    private var edgeContours: List<EdgeContour>? = null
    private var cameraDevice: CameraDevice? = null
    private val canny = FactoryEdgeDetectors.canny(
        1, true, true,
        GrayU8::class.java,
        GrayS16::class.java
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Java 1.8 issues with older SDK versions
        BoofConcurrency.USE_CONCURRENT = Build.VERSION.SDK_INT >= 24

        setContentView(R.layout.activity_main)
        setImageType(ImageType.single(GrayU8::class.java))

        val gvrSurface: GvrView = findViewById(R.id.camera_view)
        val hiddenSurface: FrameLayout = findViewById(R.id.hidden_view)

        gvrRenderer = GvrRenderer(gvrSurface, this)

        // The camera stream will now start after this function is called.
        verbose = true
        startCamera(hiddenSurface, null)
    }

    override fun configureCamera(
        device: CameraDevice?,
        characteristics: CameraCharacteristics?,
        captureRequestBuilder: CaptureRequest.Builder
    ) {
        captureRequestBuilder.set(
            CaptureRequest.CONTROL_AF_MODE,
            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO
        )
        captureRequestBuilder.set(
            CaptureRequest.CONTROL_AE_MODE,
            CaptureRequest.CONTROL_AE_MODE_ON
        )
        cameraDevice = device
    }

    override fun processImage(image: ImageBase<*>) {
        val gray = image as GrayU8
        canny.process(gray, 0.1f, 0.3f, null)
        edgeContours = canny.contours
    }

    fun Bitmap.flip(): Bitmap {
        val matrix = Matrix().apply { postScale(-1f, 1f, width/2f, width/2f) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    override fun renderBitmapImage(mode: BitmapMode?, image: ImageBase<*>?) {
        VisualizeImageData.drawEdgeContours(edgeContours, 0xFFFFFF, bitmapWork, bitmapTmp)
        if (bitmapLock.tryLock()) {
            try {
                val tmp = bitmapWork
                bitmapWork = bitmap
                bitmap = tmp
            } finally {
                bitmapLock.unlock()
            }
        }
        gvrRenderer!!.bitmapImage = bitmapWork.flip()
    }

    override fun onSurfaceTextureCreated(surfaceTexture: SurfaceTexture) {
        this.surfaceTexture = surfaceTexture
    }

    companion object {
        private val TAG = "MainActivity"
    }
}
